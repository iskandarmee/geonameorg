package com.example.geonameorg;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


import java.util.List;

public class AdapterData extends RecyclerView.Adapter<AdapterData.ViewHolder> {
    private Context context;
    private List<ModelGeoname> list;

    public AdapterData(Context context, List<ModelGeoname> list) {
        this.context = context;
        this.list = list;
    }


    @NonNull
    @Override
    public AdapterData.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View viewItem = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_data, parent, false);

        return new ViewHolder(viewItem);
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterData.ViewHolder holder, int position) {

        ModelGeoname modelGeoname = list.get(position);
        holder.tvlng.setText(String.valueOf(modelGeoname.getLng()));
        holder.tvobservation.setText(modelGeoname.getObservation());
        holder.tvICAO.setText(modelGeoname.getiCAO());
        holder.tvclouds.setText(modelGeoname.getCloudsCode());
        holder.tvdewPoint.setText(modelGeoname.getDewPoint());
        holder.tvcloudsCode.setText(modelGeoname.getCloudsCode());
        holder.tvdatetime.setText(modelGeoname.getDatetime());
        holder.tvtemperature.setText(modelGeoname.getTemperature());
        holder.tvhumidity.setText(String.valueOf(modelGeoname.getHumidity()));
        holder.tvstationName.setText(modelGeoname.getStationName());
        holder.tvweatherCondition.setText(modelGeoname.getWeatherCondition());
        holder.tvwindDirection.setText(String.valueOf(modelGeoname.getWindDirection()));
        holder.tvwindSpeed.setText(modelGeoname.getWindSpeed());
        holder.tvlat.setText(String.valueOf(modelGeoname.getLat()));
        holder.tvweatherConditionCode.setText(modelGeoname.getWeatherConditionCode());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        LinearLayout linearLayout;
        TextView tvlng, tvobservation, tvICAO, tvclouds, tvdewPoint, tvcloudsCode, tvdatetime, tvtemperature,
                tvhumidity, tvstationName, tvweatherCondition, tvwindDirection, tvwindSpeed, tvlat,
                tvweatherConditionCode, tvseaLevelPressure, tvhectoPascAltimeter;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            linearLayout = itemView.findViewById(R.id.rlCard);
            tvlng = itemView.findViewById(R.id.tvlng);
            tvobservation = itemView.findViewById(R.id.tvobservation);
            tvICAO = itemView.findViewById(R.id.tvICAO);
            tvclouds = itemView.findViewById(R.id.tvclouds);
            tvdewPoint = itemView.findViewById(R.id.tvdewPoint);
            tvcloudsCode = itemView.findViewById(R.id.tvcloudsCode);
            tvdatetime = itemView.findViewById(R.id.tvdatetime);
            tvtemperature = itemView.findViewById(R.id.tvtemperature);
            tvhumidity = itemView.findViewById(R.id.tvhumidity);
            tvstationName = itemView.findViewById(R.id.tvstationName);
            tvweatherCondition = itemView.findViewById(R.id.tvweatherCondition);
            tvwindDirection = itemView.findViewById(R.id.tvwindDirection);
            tvwindSpeed = itemView.findViewById(R.id.tvwindSpeed);
            tvlat = itemView.findViewById(R.id.tvlat);
            tvweatherConditionCode = itemView.findViewById(R.id.tvweatherConditionCode);
            tvseaLevelPressure = itemView.findViewById(R.id.tvseaLevelPressure);
            tvhectoPascAltimeter = itemView.findViewById(R.id.tvhectoPascAltimeter);
        }
    }
}
