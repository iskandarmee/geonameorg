package com.example.geonameorg;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ModelObject {
    @SerializedName("weatherObservations")
    @Expose
    private List<ModelGeoname> weatherObservations = null;

    @SerializedName("status")
    @Expose
    private ModelMessages status;

    public ModelMessages getStatus() {
        return status;
    }

    public void setStatus(ModelMessages status) {
        this.status = status;
    }

    public List<ModelGeoname> getWeatherObservations() {
        return weatherObservations;
    }

    public void setWeatherObservations(List<ModelGeoname> weatherObservations) {
        this.weatherObservations = weatherObservations;
    }
}
