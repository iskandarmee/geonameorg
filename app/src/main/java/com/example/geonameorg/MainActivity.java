package com.example.geonameorg;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.geonameorg.GetData;
import com.example.geonameorg.ModelGeoname;
import com.example.geonameorg.ModelObject;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {
    private GetData getDataService;
    private RecyclerView recyclerView;
    private ModelObject modelObject;
    private ProgressDialog progressDialog;
    private List<ModelGeoname> listGeonames;
    private AdapterData adapterGeoname;
    private Button btnGetData;
    private ModelMessages modelMessage;
    EditText etNorth, etSouth, etEst, etWest, etUsername;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setupView();

//        showData();
    }

    private void showData() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Loading ....");
        progressDialog.show();

        String valueNorth = etNorth.getText().toString();
        String valueSouth = etSouth.getText().toString();
        String valueEast = etEst.getText().toString();
        String valueWest = etWest.getText().toString();
        String valueUsername = etUsername.getText().toString();

        if (!valueNorth.isEmpty() && !valueSouth.isEmpty() && !valueEast.isEmpty() && !valueWest.isEmpty() && !valueUsername.isEmpty()){

            String url = "http://api.geonames.org/weatherJSON?north=" + valueNorth + "&south=" + valueSouth + "&east=" + valueEast + "&west=" + valueWest + "&username=" + valueUsername;
            /*String url = "http://api.geonames.org/weatherJSON?north=44.1&south=-9.9&east=-22.4&west=55.2&username=demos";*/

            getDataService = GetAPI.getRetrofitInstance().create(GetData.class);
            getDataService.getAllDataList(url)
                    .enqueue(new Callback<ModelObject>() {
                        @Override
                        public void onResponse(Call<ModelObject> call, Response<ModelObject> response) {
                            progressDialog.dismiss();
                            modelObject = response.body();
                            if (modelObject != null) {
                                listGeonames = modelObject.getWeatherObservations();
                                showAllData(listGeonames);
                                Toast.makeText(MainActivity.this, "Response", Toast.LENGTH_SHORT).show();
                            } else {
                                String message = "the hourly limit of 1000 credits for demo has been exceeded. Please use an application specific account. Do not use the demo account for your application.";
                                Toast.makeText(MainActivity.this, ""+message, Toast.LENGTH_SHORT).show();

                            }

                        }

                        @Override
                        public void onFailure(Call<ModelObject> call, Throwable t) {
                            progressDialog.dismiss();
                            Toast.makeText(MainActivity.this, "Failure", Toast.LENGTH_SHORT).show();
                        }
                    });
        } else {
            progressDialog.dismiss();
            Toast.makeText(this, "Lengkapi data", Toast.LENGTH_SHORT).show();
        }
    }

    private void showAllData(List<ModelGeoname> listGeonames) {
        recyclerView.setLayoutManager(new LinearLayoutManager(
                this,
                RecyclerView.VERTICAL,
                false));
        adapterGeoname   = new AdapterData(this, listGeonames);
        recyclerView.setAdapter(adapterGeoname);
    }

    private void setupView() {
        recyclerView = findViewById(R.id.rvGeonameData);
        etNorth = findViewById(R.id.etNorth);
        etSouth = findViewById(R.id.etSouth);
        etEst = findViewById(R.id.etEast);
        etWest = findViewById(R.id.etWest);
        etUsername = findViewById(R.id.etUsername);
        btnGetData = findViewById(R.id.btnGet);
        btnGetData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showData();
            }
        });

    }
}
