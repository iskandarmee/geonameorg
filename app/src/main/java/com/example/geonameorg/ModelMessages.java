package com.example.geonameorg;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ModelMessages {
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("value")
    @Expose
    private Integer value;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }
}
