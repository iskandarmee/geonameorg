package com.example.geonameorg;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;
import retrofit2.http.Url;

public interface GetData {
    @GET("weatherObservations")
    Call<List<ModelGeoname>> getAllData(@Query("north") String north,
                                        @Query("south") String south,
                                        @Query("east") String east,
                                        @Query("west") String west,
                                        @Query("username") String username);

    @GET
    Call<ModelObject> getAllDataList(@Url String url);
}
