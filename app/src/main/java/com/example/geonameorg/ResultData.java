package com.example.geonameorg;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import java.util.ArrayList;
import java.util.List;

public class ResultData extends AppCompatActivity {

    RecyclerView recyclerView;
    AdapterData adapterData;
    private List<ModelGeoname> list = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result_data);

        setupView();
        loadData();
        showData();
    }

    private void showData() {
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext(), RecyclerView.VERTICAL, false));
        adapterData = new AdapterData(this, list);
        recyclerView.setAdapter(adapterData);
    }

    private void loadData() {

    }

    private void setupView() {
        recyclerView = findViewById(R.id.rvResultData);

    }
}
